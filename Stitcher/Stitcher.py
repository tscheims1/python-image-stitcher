# import the necessary packages
import numpy as np
import imutils
import cv2
import scipy.linalg
import math
import numpy.linalg
from calcRotTransMatrix import calcRotTransMatrix
class Stitcher:
	def __init__(self):
		# determine if we are using OpenCV v3.X
		self.isv3 = imutils.is_cv3()
		self.imSize =(0,0)

	def stitch(self, images,imagesFull, ratio=0.75, reprojThresh=4.0,
		showMatches=False):
		# unpack the images, then detect keypoints and extract
		# local invariant descriptors from them
		matStore = []
		self.imSize = images[0].shape
		
		imageRatio = imagesFull[0].shape[0]/(images[0].shape[0]*1.0)
		for key in xrange(0,len(images)-1):
			imageB = images[key]
			imageA = images[key+1]
			
			
			(kpsA, featuresA) = self.detectAndDescribe(imageA)
			(kpsB, featuresB) = self.detectAndDescribe(imageB)

			# match features between the two images
			M = self.matchKeypoints(kpsA, kpsB,
				featuresA, featuresB, ratio, reprojThresh)
			
			print("Image "+str(key+1)+ " features extracted")

			# if the match is None, then there aren't enough matched
			# keypoints to create a panorama
			if M is None:
				return None

			# otherwise, apply a perspective warp to stitch the images
			# together
			(matches, H) = M
		
			print("local scale "+str(math.sqrt(H[0][0]*H[0][0]+H[0][1]*H[0][1]))+"    ")

			print(H)

			if(len(matStore)):
				matStore.append(np.dot(matStore[-1],H))
			else:
				matStore.append(H)
				
				
		
		index = 1
		fullImage = imagesFull[0]
		
		globalImageTranslation = np.array([0.0,0.0])
		for mat in matStore:
		
		
			print("global Scale "+str(math.sqrt(mat[0][0]*mat[0][0]+mat[0][1]*mat[0][1]))+"    ")
			print("global Scale "+str(math.sqrt(mat[1][0]*mat[1][0]+mat[1][1]*mat[1][1]))+"    ")
			
			
			ro1 = math.atan2(-mat[0][1],mat[0][0])
			ro2 = math.atan2(-mat[1][0],mat[1][1])
			
			print(ro1)
			print(ro2)
			
			print(mat)
			#calculate size of new images
			pointUpperRight = np.array([[imagesFull[index].shape[1]],[imagesFull[index].shape[0]],[1]])
			pointUpperLeft = np.array([[0],[imagesFull[index].shape[0]],[1]])
			pointBottomRight = np.array([[imagesFull[index].shape[1]],[0],[1]])
			pointBottomLeft = np.array([[0],[0],[1]])
			print(numpy.linalg.norm(pointBottomLeft-pointBottomRight))
			print(numpy.linalg.norm(pointBottomRight-pointUpperRight))
		
		
			A = np.dot(mat,pointBottomLeft)
			B = np.dot(mat,pointBottomRight)
			C = np.dot(mat,pointUpperRight)
			D = np.dot(mat,pointUpperLeft)
			
			
			print(numpy.linalg.norm(A-B))
			print(numpy.linalg.norm(B-C))
			print(numpy.linalg.norm(C-D))

			
			
			
			maxX = max(A[0],B[0],C[0],D[0])
			minX = min(A[0],B[0],C[0],D[0])
			maxY = max(A[1],B[1],C[1],D[1])
			minY = min(A[1],B[1],C[1],D[1])
		
			
			#translate Transformaton if some coordinates are negative
			shiftX = 0
			shiftY = 0
			if minX < 0:
				shiftX = -minX
				maxX += shiftX
			if minY < 0:
				shiftY = -minY
				maxY += shiftY
			
			translateMat =  np.array([[1.0,0,shiftX],
									[0,1.0,shiftY],
										[0,0,1.0]])
			mat = np.dot(translateMat,mat)
			
			print(mat)
			print(cv2.determinant(mat))
			result =  cv2.warpAffine(imagesFull[index], np.resize(mat,(2,3)),
			(maxX,maxY))
			
			print("Image "+str(index)+ " transformed")
			
			
			maxShape1 = max(result.shape[1],fullImage.shape[1])
			maxShape0 = max(result.shape[0],fullImage.shape[0])			
			

			
			#the global Translation musst only shift the delta
			if(shiftX > globalImageTranslation[0]):
				shiftX -= globalImageTranslation[0]
			else:
				shiftX = 0
				
			if(shiftY > globalImageTranslation[1]):
				shiftY -= globalImageTranslation[1]
			else:
				shiftY= 0

			
			fullImageShape = np.zeros((maxShape0+int(shiftY),maxShape1+int(shiftX),3))

			
			fullImage = cv2.warpAffine(fullImage,np.array([[1.0,0,shiftX],[0,1.0,shiftY]]),(fullImageShape.shape[1],fullImageShape.shape[0]))
			globalImageTranslation[0] += shiftX
			globalImageTranslation[1] += shiftY
			
			
			locs = np.where(result != 0) 
			
			for i in xrange(0,len(locs[0])):
				
				x = locs[0][i]
				y = locs[1][i]
				z = locs[2][i]
				fullImage[x][y][z] = result [x][y][z]
			
			print("Image "+str(index)+ " stitched")

			index+=1

		return (fullImage)

	def detectAndDescribe(self, image):
		# convert the image to grayscale
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		# check to see if we are using OpenCV 3.X
		if self.isv3:
			# detect and extract features from the image
			descriptor = cv2.xfeatures2d.SIFT_create()
			(kps, features) = descriptor.detectAndCompute(image, None)

		# otherwise, we are using OpenCV 2.4.X
		else:
			# detect keypoints in the image
			detector = cv2.FeatureDetector_create("SIFT")
			kps = detector.detect(gray)

			# extract features from the image
			extractor = cv2.DescriptorExtractor_create("SIFT")
			(kps, features) = extractor.compute(gray, kps)

		# convert the keypoints from KeyPoint objects to NumPy
		# arrays
		kps = np.float32([kp.pt for kp in kps])

		# return a tuple of keypoints and features
		return (kps, features)

	def matchKeypoints(self, kpsA, kpsB, featuresA, featuresB,
		ratio, reprojThresh):
		# compute the raw matches and initialize the list of actual
		# matches
		matcher = cv2.DescriptorMatcher_create("BruteForce")
		rawMatches = matcher.knnMatch(featuresA, featuresB, 2)
		matches = []

		# loop over the raw matches
		for m in rawMatches:
			# ensure the distance is within a certain ratio of each
			# other (i.e. Lowe's ratio test)
			if len(m) == 2 and m[0].distance < m[1].distance * ratio:
				matches.append((m[0].trainIdx, m[0].queryIdx))

		# computing a homography requires at least 4 matches
		if len(matches) > 4:
			# construct the two sets of points
			ptsA = np.float32([kpsA[i] for (_, i) in matches])
			ptsB = np.float32([kpsB[i] for (i, _) in matches])

			# compute the homography between the two sets of points
			#(H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC,
			#	reprojThresh)
			
			"""
			R,T= calcRotTransMatrix(np.transpose(ptsA),np.transpose(ptsB))
			
			
			H = np.zeros((3,3),dtype=float)
			H[0:2,0:2] = R
			H[0:2,2] = T
			H[2][2] = 1
			"""
			
			
			H = cv2.estimateRigidTransform(ptsA, ptsB,False)
			status = 1
			H.resize(3,3)
			H[2][2] = 1
			
			centerPoint = np.array([[self.imSize[1]/2.0],[self.imSize[0]/2.0],[1]])
			
			#calculate the transformation of the center point
			#without scaling correction
			oldPoint = np.dot(H,centerPoint)
			
			#calculate the uniform Scaling
			uniformScale = math.sqrt(H[0][0]*H[0][0]+H[0][1]*H[0][1])
			
			print(uniformScale)
			
			#create a transform matrix with scaling = 1
			H = np.dot(H,np.array([[(1/uniformScale),0,0],
								[0,(1/uniformScale),0],
								[0,0,1]]))
			print(math.sqrt(H[0][0]*H[0][0]+H[0][1]*H[0][1]))
			newPoint = np.dot(H,centerPoint)
			
			#calculate the mean translation of the scaling (translation of the center point)
			deltaPoint = newPoint - oldPoint
			print(deltaPoint)
			H[0][2] -= deltaPoint[0]
			H[1][2] -= deltaPoint[1]

			print(H)

			# return the matches along with the homograpy matrix
			# and status of each matched point
			return (matches, H)

		# otherwise, no homograpy could be computed
		return None

	def drawMatches(self, imageA, imageB, kpsA, kpsB, matches, status):
		# initialize the output visualization image
		(hA, wA) = imageA.shape[:2]
		(hB, wB) = imageB.shape[:2]
		vis = np.zeros((max(hA, hB), wA + wB, 3), dtype="uint8")
		vis[0:hA, 0:wA] = imageA
		vis[0:hB, wA:] = imageB

		# loop over the matches
		for ((trainIdx, queryIdx), s) in zip(matches, status):
			# only process the match if the keypoint was successfully
			# matched
			if s == 1:
				# draw the match
				ptA = (int(kpsA[queryIdx][0]), int(kpsA[queryIdx][1]))
				ptB = (int(kpsB[trainIdx][0]) + wA, int(kpsB[trainIdx][1]))
				cv2.line(vis, ptA, ptB, (0, 255, 0), 1)

		# return the visualization
		return vis
