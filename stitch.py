#stitches all given images together 

# import the necessary packages
from Stitcher import Stitcher
import argparse
import imutils
import cv2
import numpy as np
 

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()


ap.add_argument('files', type=str, nargs='+',
                    help='files to stitch')


args = vars(ap.parse_args())


map1 = []
map2 = []
rimage = []

images = []
imagesFull = []
for  value in args['files']:
	print("load "+str(value))
	img  = cv2.imread(value)
	

	imgResized = imutils.resize(img,width=1000)
	imgResized2 = imutils.resize(img,width=1000)
	images.append(imgResized)
	imagesFull.append(imgResized2)
	
	


stitcher = Stitcher()
(fullimage) = stitcher.stitch(images,imagesFull, showMatches=True)

cv2.imwrite("fullimage.png",fullimage)
